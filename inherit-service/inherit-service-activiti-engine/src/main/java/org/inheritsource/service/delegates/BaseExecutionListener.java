package org.inheritsource.service.delegates;

import java.util.logging.Logger;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class BaseExecutionListener implements ExecutionListener {
	public static final Logger log = Logger.getLogger(BaseExecutionListener.class.getName());
	
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		log.info("Execution ID [" + execution.getId() + "] processInstance ID [" + execution.getProcessInstanceId() + "]");
	}

}
