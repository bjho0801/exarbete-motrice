package org.inheritsource.service.delegates;

import java.net.URI;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 * RESTful implementation of BPMN 2.0 message task. A RESTful message
 * task is based on the REST constraint of an uniform interface, specifically
 * the use of self-descriptive messages. A message must contain:
 * a target URI (URI),
 * the HTTP-method used (String),
 * a specified media type (String), 
 * the representation to transfer (String).
 * These should be provided as variables to the execution.  
 * The response is stored in the return variable.
 * 
 * @author bwrobert
 *
 */
public class MessageTaskDelegate implements JavaDelegate {
	
	public static final Logger log = Logger.getLogger(MessageTaskDelegate.class.getName());

	@Override
	public void execute(DelegateExecution execution) throws Exception {
			
		URI targetURI = UriBuilder
						.fromUri((URI)execution.getVariable("targetURI"))
						.build();
		String httpMethod = (String)execution.getVariable("httpMethod");
		httpMethod = httpMethod.toUpperCase();
		String mediaType = (String)execution.getVariable("mediaType");
		mediaType = mediaType.toLowerCase();
		String representation = (String)execution.getVariable("representation");
		log.info(httpMethod + " " + targetURI.toString() + "\n" + mediaType + " " + representation);
		
		Client client = Client.create();
		WebResource wr = client.resource(targetURI);
		String resp = wr.type(mediaType).accept(mediaType).method(httpMethod, String.class, representation);
		log.info("RESPONSE " + resp);
	}

}
