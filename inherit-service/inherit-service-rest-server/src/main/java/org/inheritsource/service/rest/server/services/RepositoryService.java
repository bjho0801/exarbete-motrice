/* == Motrice Copyright Notice == 
 * 
 * Motrice Service Platform 
 * 
 * Copyright (C) 2011-2014 Motrice AB 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU Affero General Public License for more details. 
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * e-mail: info _at_ motrice.se 
 * mail: Motrice AB, Långsjövägen 8, SE-131 33 NACKA, SWEDEN 
 * phone: +46 8 641 64 14 
 
 */ 
 
package org.inheritsource.service.rest.server.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.inheritsource.taskform.engine.TaskFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/repository")
public class RepositoryService {
	
	public static final Logger log = Logger.getLogger(RepositoryService.class.getName());
		
	@Autowired
	TaskFormService engine;
	
	@Context
	UriInfo uriInfo;
	
	/**
	 * Get all deployed processes.
	 * @return
	 */
	@GET
	@Path("/processes")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProcessInstances() {
		List<ProcessDefinition> pdList = engine.getActivitiEngineService().getEngine().getRepositoryService()
				.createProcessDefinitionQuery()
				.orderByDeploymentId()
				.asc()
				.list();
		String s = "";
		
		for(ProcessDefinition pd : pdList) {
			s += "Process name: " + pd.getName() + " :: Process key: " + pd.getKey() + " :: Process ID: " + pd.getId() + "\n" + " :: Process depl.ID: " + pd.getDeploymentId() + "\n";
		}
		
		return s;
	}

	/**
	 * Activates/suspends a process definition with the given ID. If no body containing the key-value pair
	 * @param The ID of the process definition
	 * @return 201 with the HTTP header Location set to the URI of the process instance if created.
	 * @return 400 if no process with the specified ID is found.
	 */
	@PUT
	@Path("processes/id/{id}")
	@Consumes({MediaType.TEXT_PLAIN, "application/xml","application/json"})
	@Produces({MediaType.TEXT_PLAIN})
	public Response activateProcessById(@PathParam("id") String processId) {
		log.fine("REST call to activate/suspend process definition with ID=[" + processId + "]");
		
		ProcessDefinition pd = null;
	
		try {
			pd = engine.getActivitiEngineService().getEngine().getRepositoryService()
					.createProcessDefinitionQuery()
					.processDefinitionId(processId)
					.singleResult();
		} catch(ActivitiException e) {
			return Response.status(404).build();
		}
		
		
		// Activate/Suspend process definition
		String body = "";
		try {
			if(pd.isSuspended()) {
				engine.getActivitiEngineService().getEngine().getRepositoryService().activateProcessDefinitionById(processId);
				body = "Activated";
			}
			else {
				engine.getActivitiEngineService().getEngine().getRepositoryService().suspendProcessDefinitionById(processId);
				body = "Suspended";
			}
		} catch(ActivitiObjectNotFoundException e) {
			return Response.status(400).build();
		}

		return Response.status(200).entity(body).build();
	}
	
	/**
	 * Activates/suspends a process definition with the given key.
	 * @param The key of the process definition
	 * @return 200 if the process definition is activated.
	 * @return 400 if no process with the specified ID is found.
	 */
	@PUT
	@Path("processes/key/{key}")
	@Consumes({MediaType.TEXT_PLAIN, "application/xml","application/json"})
	@Produces({MediaType.TEXT_PLAIN})
	public Response activateProcessByKey(@PathParam("key") String key) {
		ProcessInstance pi = null;
		log.fine("REST call to activate/suspend process definition with key=[" + key + "]");
		System.out.println("REST call to activate/suspend process definition with key=[" + key + "]");
		ProcessDefinition pd = null;
		try {
			pd = engine.getActivitiEngineService().getEngine().getRepositoryService()
			.createProcessDefinitionQuery()
			.processDefinitionKey(key).latestVersion().singleResult();
		} catch(ActivitiException e) {
			return Response.status(404).build();
		}


		// Activate/Suspend process definition
		String body = "";
		try {
			if(pd.isSuspended()) {
				engine.getActivitiEngineService().getEngine().getRepositoryService().activateProcessDefinitionByKey(key);
				body = "Activated";
			}
				
			else {
				engine.getActivitiEngineService().getEngine().getRepositoryService().suspendProcessDefinitionByKey(key);
				body = "Suspended";
			}	
		} catch(ActivitiObjectNotFoundException e) {
			return Response.status(400).build();
		}

		return Response.status(200).build();
	}
	
}
