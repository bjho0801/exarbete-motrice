package org.inheritsource.service.rest.server.services.location;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import org.hibernate.service.spi.Wrapped;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.XML;

@Produces(MediaType.APPLICATION_XML)
public class LocationService {
	
	@GET
	public String getXml() {
		return new String("<test><entry label=\"abc\" value=\"this is abc\" /><entry label=\"def\" value=\"this is def\" /></test>");
	}
	
	@GET
	@Path("addresses")
	public String getAddressesAsXml(@QueryParam("q") String address) {
		if(address.length() < 3 )
			return null;
		
		Client client = Client.create();
		WebResource wr = client.resource("http://xyz.malmo.se/WS/mKarta/autocomplete.ashx");
		String resp = wr.queryParam("q", address)
						.queryParam("limit", "5000")
						.accept(MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML)
						.get(String.class);
		String a[] = resp.split("[\\r\\n]+");
		
		String ret = "<root>";
		for(String s : a )
			ret += "<entry>" + s + "</entry>";
		ret += "</root>";
		
		return ret;
	}
	
	@GET
	@Path("properties")
	public String getPropertiesAsXml(@QueryParam("q") String address) {
		if(address.length() < 3)
			return null;
		
		Client client = Client.create();
		WebResource wr = client.resource("http://xyz.malmo.se/WS/mKarta/sokexakt.ashx");
		String resp = wr.queryParam("q", address)
					  .accept(MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON)
					  .get(String.class);
		JSONObject json = new JSONObject(resp);
		String xml  = XML.toString(json);
		return null;
	}
}
