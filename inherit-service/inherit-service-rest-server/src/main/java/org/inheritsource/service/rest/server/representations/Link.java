/* == Motrice Copyright Notice == 
 * 
 * Motrice Service Platform 
 * 
 * Copyright (C) 2011-2014 Motrice AB 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU Affero General Public License for more details. 
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * e-mail: info _at_ motrice.se 
 * mail: Motrice AB, Långsjövägen 8, SE-131 33 NACKA, SWEDEN 
 * phone: +46 8 641 64 14 
 
 */

package org.inheritsource.service.rest.server.representations;

import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class representing a hyperlink inspired by the HTML5 <LINK> element
 * and the "data-*" attribute extension specified in the HTML 5.1 specification
 * draft (http://www.w3.org/TR/html51/). As per the specification the
 * attributes "rel", "href", "type", "title" keep their semantics.
 * "data-method" indicates the HTTP-method to use when interacting with
 * the resource whereas "data-message" indicates the message to send.
 * 
 * Links are build using the inner static class Builder.
 * @author bwrobert
 *
 */
@XmlRootElement(namespace = Representation.MOTRICE_NAMESPACE)
public class Link {
    @XmlAttribute(name = "rel")
    private String rel;
    @XmlAttribute(name = "href")
    private  String uri;
    @XmlAttribute(name = "type")
    private String mediaType;
    @XmlAttribute(name = "title")
    private String title;
    @XmlAttribute(name = "data-method")
    private String method;
    @XmlAttribute(name = "data-message")
    private String message;
    
    public static class Builder {
    	// Required as per the HTML5 spec: http://www.w3.org/TR/html5/document-metadata.html#the-link-element
    	private final String rel;
    	private final String uri;
    	
    	// Optional
    	private String mediaType = Representation.MOTRICE_MEDIA_TYPE;
    	private String title;
    	
    	// Extensions that are optional
    	private String method;
    	private String message;
    	
    	public Builder(String rel, URI uri) {
    		this.rel = rel;
    		this.uri = uri.toString();
    	}
    	
    	public Builder mediaType(String mediaType) {
    		this.mediaType = mediaType; return this;
    	}
    	
    	public Builder title(String title) {
    		this.title = title; return this;
    	}
    	
    	public Builder method(String method) {
    		this.method = method.toUpperCase(); return this;
    	}
    	
    	public Builder message(String message) {
    		this.message = message; return this;
    	}
    	
    	public Link build() {
    		return new Link(this);
    	}
    }
    
    private Link() {}
    
    private Link(Builder builder) {
    	this.uri = builder.uri;
    	this.rel = builder.rel;
    	this.mediaType = builder.mediaType;
    	this.title = builder.title;
    	this.method = builder.method;
    	this.message = builder.message;
    }

    public URI getURI() {
    	try {
    		return new URI(this.uri);
    	} catch(URISyntaxException e) {
    		
    	}
    	return null;
    }  
    
    public String getRelValue() {
    	return this.rel;
    }
    
    public String getMediaType() {
    	return this.mediaType;
    }
    
    public String getTitle() {
    	return this.title;
    }
    
    public String getMethod() {
    	return this.method.toUpperCase();
    }
    
    public String getMessage() {
    	return this.message;
    }
}
