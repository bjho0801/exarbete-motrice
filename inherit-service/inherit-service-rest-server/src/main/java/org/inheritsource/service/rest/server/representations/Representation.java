/* == Motrice Copyright Notice == 
 * 
 * Motrice Service Platform 
 * 
 * Copyright (C) 2011-2014 Motrice AB 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU Affero General Public License for more details. 
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * e-mail: info _at_ motrice.se 
 * mail: Motrice AB, Långsjövägen 8, SE-131 33 NACKA, SWEDEN 
 * phone: +46 8 641 64 14 
 
 */

package org.inheritsource.service.rest.server.representations;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.inheritsource.service.rest.server.representations.Link;

public abstract class Representation {
	public static final String RELATIONS_URI = "http://relations.motrice.org";
	public static final String MOTRICE_NAMESPACE = "http://schemas.motrice.org";
	public static final String MOTRICE_MEDIA_TYPE = "application/vnd.motrice+xml";
	public static final String SELF_REL_VALUE = "self";
	
	@XmlElement(name = "link", namespace = MOTRICE_NAMESPACE)
	protected List<Link> links;
	
	protected Link getLinkByName(String uriName) {
		if (links == null)
			return null;
		
		for(Link l : links) {
			if(l.getRelValue().toLowerCase().equals(uriName.toLowerCase()))
				return l;
		}
		return null;
	}
}