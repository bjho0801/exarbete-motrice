/* == Motrice Copyright Notice == 
 * 
 * Motrice Service Platform 
 * 
 * Copyright (C) 2011-2014 Motrice AB 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU Affero General Public License for more details. 
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * e-mail: info _at_ motrice.se 
 * mail: Motrice AB, Långsjövägen 8, SE-131 33 NACKA, SWEDEN 
 * phone: +46 8 641 64 14 
 
 */ 

package org.inheritsource.service.rest.server.services.processes;

import java.net.URI;
import java.util.logging.Logger;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.Message;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.inheritsource.service.rest.server.services.RuntimeService;
import org.inheritsource.taskform.engine.TaskFormService;
import org.inheritsource.service.rest.server.representations.Link;
import org.inheritsource.service.rest.server.representations.ProcessRepresentation;
import org.inheritsource.service.rest.server.representations.Representation;
import org.inheritsource.service.rest.server.representations.exceptions.MediaTypeException;


/**
 * Service (resource) responsible for routing information regarding BPMN-processes from requests to activities
 * and marshalling results and exceptions into HTTP responses.
 * @author bwrobert
 *
 */

@Component /* Used by Spring */
public class ProcessService {
	
	public static final Logger log = Logger.getLogger(ProcessService.class.getName());
	
	@Autowired
	TaskFormService engine;
	
	@Context
	UriInfo uriInfo;
	
	
	/**
	 * Start a process by message as defined by the BPMN 2.0 specification. The message body of the HTTP-request
	 * must conform to the media type application/vnd.motrice+xml.
	 * @return 201 with the HTTP header Location set to the URI of the process instance if created.
	 * @return 400 if no process subscribes to the message.
	 * @return 406 if the Accept header is not application/vnd.motrice+xml.
	 * @return 415 if the Content-Type is not application/vnd.motrice+xml.
	 * @return 422 if the message body does not conform to the media type application/vnd.motrice+xml.
	 * @return 500 if any other exception occurs.
	 */
	@POST
	@Consumes({Representation.MOTRICE_MEDIA_TYPE})
	@Produces({Representation.MOTRICE_MEDIA_TYPE})
	public Response startProcessByMessage(String content) {
		/*
		BpmnModel bpm = null;
		bpm = engine.getActivitiEngineService().getEngine().getRepositoryService().getBpmnModel("proc_exarb_proc_b:1:406");
		s += "\nMessages:";
		for(Message m : bpm.getMessages()) {
			s += "name:" + m.getName() + "--ItemRef:" + m.getItemRef();
		}
		*/
		
		try {
			ProcessRepresentation prRequest = ProcessRepresentation.fromXmlString(content);
			log.info("ProcessService: REQUEST to start process by message=[" + prRequest.getMessage() + "]");
						
			ProcessInstance pi = null;
			
			/* Callback requested? Extract */
			if(prRequest.getMessageLink() != null) {
				ProcessRepresentation prResponse = new ProcessRepresentation(prRequest.getMessageLink().getMessage());
				Map<String,Object> vars = new HashMap<String, Object>();
				vars.put("targetURI", prRequest.getMessageLink().getURI());
				vars.put("httpMethod", prRequest.getMessageLink().getMethod());
				vars.put("mediaType", prRequest.getMessageLink().getMediaType());
				vars.put("representation", prResponse.toString());
				pi = engine.getActivitiEngineService().getEngine().getRuntimeService().startProcessInstanceByMessage(prRequest.getMessage(), vars);
			} else {
				pi = engine.getActivitiEngineService().getEngine().getRuntimeService().startProcessInstanceByMessage(prRequest.getMessage());
			}
			log.info("ProcessService: RESPONSE is 201 with Location=[" + uriInfo.getAbsolutePathBuilder().path(pi.getId()).build().toString() + "]");
			return Response.status(201)
					.entity(uriInfo.getAbsolutePathBuilder().path(pi.getId()).build().toString())
					.location(uriInfo.getAbsolutePathBuilder().path(pi.getId()).build())
					.build();
			
		} catch (MediaTypeException e) {
			Response.status(422).build();
		} catch(ActivitiException ae) {
			return Response.status(400).build();
		} catch(Exception e) {
			return Response.status(500).build();
		}
		return null;
	}
	
	/**
	 * Activates an process instance by message as specified by the BPMN 2.0 specification.
	 * The implementation finds the the proper processes execution by querying the engine
	 * using a provided process instance ID and a named message. The ID is provided using
	 * an URL-template while the message MUST be provided through a representation conforming
	 * to the media type application/vnd.motrice+xml. 
	 * @param The process instance ID
	 * @param An representation conforming to the media type application/vnd.motrice+xml
	 * @return
	 */
	@PUT
	@Path("/{id}")
	@Consumes({Representation.MOTRICE_MEDIA_TYPE})
	@Produces({Representation.MOTRICE_MEDIA_TYPE})
	public Response updateProcessByMessage(@PathParam("id") String id, String content){
		log.info("ProcessService: REQUEST to update process instance with ID=[" + id + "]");
		try {
			ProcessRepresentation prRequest = ProcessRepresentation.fromXmlString(content);
			Execution e = engine.getActivitiEngineService().getEngine().getRuntimeService()
								.createExecutionQuery()
								.processInstanceId(id)
								.messageEventSubscriptionName(prRequest.getMessage())
								.singleResult();
			engine.getActivitiEngineService().getEngine().getRuntimeService().messageEventReceivedAsync(prRequest.getMessage(), e.getId());
		} catch(MediaTypeException me) {
			return Response.status(422).build();
		} catch(ActivitiObjectNotFoundException aoe) {
			return Response.status(404).entity("No such process").build();
		} catch(ActivitiException ae) {
			return Response.status(400).entity("Wrong message").build();
		} catch(Exception e) {
			return Response.status(500).build();
		}
		log.info("ProcessService: RESPONSE is 200 Ok");
		return Response.status(200).build();
	}
	
	/**
	 * Creates a process instance of the process definition with the given ID.
	 * @param The ID of the process definition
	 * @return 201 with the HTTP header Location set to the URI of the process instance if created.
	 * @return 400 if no process with the specified ID is found.
	 */
	@POST
	@Path("/id/{id}")
	@Consumes({Representation.MOTRICE_MEDIA_TYPE})
	@Produces({Representation.MOTRICE_MEDIA_TYPE})
	public Response startProcessInstanceById(@PathParam("id") String processId) {
		ProcessInstance pi = null;
		log.fine("REST call to start process by ID=[" + processId + "]");	
		try {
			pi = engine.getActivitiEngineService().getEngine().getRuntimeService().startProcessInstanceById(processId);
		} catch(ActivitiException ae) {
			return Response.status(400).build();
		} catch(Exception e) {
			return Response.serverError().build();
		}

		return Response.status(201)
				.entity(uriInfo.getAbsolutePathBuilder().path(pi.getId()).build().toString())
				.location(uriInfo.getAbsolutePathBuilder().path(pi.getId()).build())
				.build();
	}
	
	/**
	 * Creates a process instance of the process definition with the given process definition key.
	 * The process definition key always refers to the latest version of the process definition.
	 * @param The key associated with the process instance
	 * @return 201 with the HTTP header Location set to the URI of the process instance if created.
	 * @return 400 if no process with the specified ID is found.
	 */
	@POST
	@Path("/key/{key}")
	@Consumes({Representation.MOTRICE_MEDIA_TYPE, "application/xml","application/json"})
	@Produces({Representation.MOTRICE_MEDIA_TYPE})
	public Response startProcessByKey(@PathParam("key") String key, String content) {
		ProcessInstance pi = null;
		try {
			pi = engine.getActivitiEngineService().getEngine().getRuntimeService().startProcessInstanceByKey(key);
			ProcessRepresentation pr = new ProcessRepresentation("start",
					new Link.Builder("message", uriInfo.getBaseUriBuilder().path(RuntimeService.class).path("processes").path(pi.getId()).build())
						.mediaType(Representation.MOTRICE_MEDIA_TYPE)
						.method("put")
						.message("update")
						.build());
			
			Map<String, Object> vars = new HashMap<String, Object>();
			//URI uri = new URI("http://192.168.1.99:8080/restrice/jersey/runtime/processes/"); 
			//vars.put("targetURI", uri);
			vars.put("targetURI", uriInfo.getBaseUriBuilder().path(RuntimeService.class).path("processes").build());
			vars.put("httpMethod", "POST");
			vars.put("mediaType", Representation.MOTRICE_MEDIA_TYPE);
			vars.put("representation", pr.toString());
			
			engine.getActivitiEngineService().getEngine().getRuntimeService().setVariables(pi.getId(), vars);
			log.info("REST executionID is now[" + pi.getId() + "] instanceID is [" + pi.getProcessInstanceId() + "]");
			engine.getActivitiEngineService().getEngine().getRuntimeService().signal(pi.getId());
		} catch(ActivitiException ae) {
			return Response.status(400).build();
		} catch(Exception e) {
			return Response.status(500).build();
		}
		return Response.status(201)
				.entity(uriInfo.getBaseUriBuilder().path(pi.getId()).build().toString())
				.location(uriInfo.getAbsolutePathBuilder().path(pi.getId()).build())
				.build();
	}
	
	@Path("/message/{message}")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getMessageSubscriptions(@PathParam("message") String message) {
		List<Execution> executions = engine.getActivitiEngineService().getEngine().getRuntimeService().createExecutionQuery()
			      .messageEventSubscriptionName(message).list();
		String s = "";
		for(Execution e : executions) {
			s += " " + e.getId();
		}
		return Response.ok(s).build();
	}
	
	@Path("/message/{message}/{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateProcessInstanceByMessage(@PathParam("message") String message, @PathParam("id") String id) {
		try {
			engine.getActivitiEngineService().getEngine().getRuntimeService().messageEventReceived(message, id);
		} catch (ActivitiObjectNotFoundException aoe) {
			return Response.status(404).entity("No such process").build();
		} catch (ActivitiException ae) {
			return Response.status(400).entity("Wrong message").build();
		}
		return Response.ok().build();
	}
	
}
