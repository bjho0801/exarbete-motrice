package org.inheritsource.service.rest.server.representations;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.inheritsource.service.rest.server.representations.exceptions.MediaTypeException;


@XmlRootElement(name="process", namespace=Representation.MOTRICE_NAMESPACE)
public class ProcessRepresentation extends Representation {
	@XmlElement(name="message", namespace=Representation.MOTRICE_NAMESPACE)
	private String message;
	
	ProcessRepresentation() {}
	
	/**
	 * Create a new ProcessRepresentation from a string.
	 * @param the String to parse 
	 * @return a new ProcessRepresentation
	 * @throws MediaTypeException
	 */
	public static ProcessRepresentation fromXmlString(String xmlRepresentation) throws MediaTypeException{
		try {
			JAXBContext context = JAXBContext.newInstance(ProcessRepresentation.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			return (ProcessRepresentation)unmarshaller.unmarshal( new ByteArrayInputStream(xmlRepresentation.getBytes())); 
		}catch(Exception e){
			throw new MediaTypeException("Unable to parse representation.");
		}
	}
	
	public ProcessRepresentation(String message) {
		this.message = message;
	}
	
	public ProcessRepresentation(String message, Link...links) {
		this.message = message;
		this.links = java.util.Arrays.asList(links);
	}
	
	public String getMessage() {
		return message;
	}
	
	public Link getMessageLink() {
		return this.getLinkByName("message");
	}
	
	public String toString() {
		try {
			JAXBContext context = JAXBContext.newInstance(ProcessRepresentation.class);
			Marshaller marshaller = context.createMarshaller();
			StringWriter stringWriter = new StringWriter();
			marshaller.marshal(this, stringWriter);
			return stringWriter.toString();
		}catch(Exception e) {
			return null;
		}
	}

}
