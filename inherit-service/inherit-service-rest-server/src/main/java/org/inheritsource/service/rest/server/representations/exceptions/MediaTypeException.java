package org.inheritsource.service.rest.server.representations.exceptions;

public class MediaTypeException extends Exception {
	private static final long serialVersionUID = 289764012813675211L;

	public MediaTypeException(String message) {
		super(message);
	}
}
