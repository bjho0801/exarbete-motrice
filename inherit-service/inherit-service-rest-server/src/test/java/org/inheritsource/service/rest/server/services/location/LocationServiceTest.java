package org.inheritsource.service.rest.server.services.location;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

import javax.ws.rs.core.Response;

import org.junit.Test;

import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.AppDescriptor;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

public class LocationServiceTest extends JerseyTest {
	
	@Override
	protected AppDescriptor configure() {
		return new WebAppDescriptor.Builder().build();
	}
	
	@Test
	public void sourceServerShouldRespond() {
		WebResource wr = client().resource("http://xyz.malmo.se/WS/mKarta/sokexakt.ashx");
		Response s = wr.queryParam("q", "nobeltorget").get(Response.class);
		assertThat(s.getStatus(), is(200));
	}
	

}
