Script for localizing Orbeon Forms (the Form Runner) to Swedish

The input is the jar file in the 'sv' directory.
It originates from the Orbeon source tree.

Run the script 'localize-sv.sh' on the main Orbeon war (orbeon.war).
